Name:          abrt
Version:       2.14.2
Release:       2
Summary:       A tool for automatic bug detection and reporting
License:       GPLv2+
URL:           https://abrt.readthedocs.org/
Source:        https://github.com/abrt/%{name}/archive/%{version}/%{name}-%{version}.tar.gz

Patch0001:     0001-applet-Pass-instance-pointer-to-signal-handler.patch 
Patch0002:     0002-applet-Chain-up-in-dispose.patch
Patch0003:     0003-applet-application-Fix-crash-when-processing-deferre.patch

BuildRequires: git-core dbus-devel hostname gtk3-devel glib2-devel >= 2.43.4 rpm-devel >= 4.6
BuildRequires: desktop-file-utils libnotify-devel gettext libxml2-devel intltool libtool
BuildRequires: libsoup-devel asciidoc doxygen xmlto libreport-devel >= 2.13.0 python3-pytest
BuildRequires: satyr-devel >= 0.24 augeas libselinux-devel python3-devel python3-systemd
BuildRequires: python3-nose python3-sphinx python3-libreport python3-devel python3-argcomplete
BuildRequires: libreport-gtk-devel >= 2.13.0 gsettings-desktop-schemas-devel >= 3.15
BuildRequires: gdb-headless libcap-devel systemd-devel json-c-devel  gdb-headless polkit-devel


Requires:      libreport >= 2.13.0 satyr >= 0.24
Requires:      systemd python3-%{name} = %{version}-%{release} python3-augeas python3-dbus
Requires:      dmidecode
Requires:      %{name}-libs = %{version}-%{release}

Requires(pre): shadow-utils
%{?systemd_requires}

%description
Abrt is an automatic bug detection and reporting tool, it is used to create
a bug report with all information needed by maintainer to fix it. It extends its
functionality by using plugin system.

%package libs
Summary:   Libraries for abrt

%description libs
Libraries for abrt.

%package devel
Summary:   Development files for the abrt library
Requires:  %{name}-libs = %{version}-%{release}

%description devel
This package contains development and header files for abrt.

%package help
Summary:   Help files for the abrt library
Requires:  %{name} = %{version}-%{release}

%description help
Help document files for abrt library.


%package gui
Summary:   GUI module for abrt
Requires:  %{name} = %{version}-%{release} %{name}-dbus = %{version}-%{release}
Requires:  gnome-abrt gsettings-desktop-schemas >= 3.15
Requires:  %{name}-libs = %{version}-%{release}
Provides:  abrt-gui-libs = %{version}-%{release} abrt-applet = %{version}-%{release}
Obsoletes: abrt-gui-libs < 2.13.0-2 abrt-applet < 0.0.5
Conflicts: abrt-applet < 0.0.5

%description gui
The abrt-gui package is a GTK+ wizard to make bug reporting more conveniently.

%package gui-devel
Summary:   Development libraries for %{name}-gui
Requires:  abrt-gui = %{version}-%{release}

%description gui-devel
This package contains development and header files for abrt-gui.

%package addon-ccpp
Summary:   C/C++ addon module for abrt
Requires:  cpio gdb-headless elfutils %{name} = %{version}-%{release} python3-libreport
Requires:  %{name}-libs = %{version}-%{release}
Obsoletes: abrt-addon-coredump-helper <= 2.12.2

%description addon-ccpp
C/C++ analyzer plugin for abrt.

%package addon-upload-watch
Summary:  Upload addon module for abrt
Requires: %{name} = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description addon-upload-watch
This addon-upload-watch package provides hook for uploaded problems.

%package retrace-client
Summary:  retrace client module for abrt
Requires: %{name} = %{version}-%{release} xz tar p11-kit-trust libsoup

%description retrace-client
Retrace server's client application that helps to analyze C/C++ crashes remotely.

%package addon-kerneloops
Summary:  Kerneloops addon for abrt
Requires: curl %{name} = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description addon-kerneloops
This package provides plugin which helps to  collect kernel crash information
from system log.

%package addon-xorg
Summary:  Xorg addon module for abrt
Requires: curl %{name} = %{version}-%{release}
Requires: %{name}-libs = %{version}-%{release}

%description addon-xorg
This package provides plugin which helps to  collect Xorg crash information
from Xorg log.

%package addon-vmcore
Summary:  Vmcore addon module for abrt
Requires: %{name} = %{version}-%{release} abrt-addon-kerneloops kexec-tools
Requires: python3-abrt python3-augeas util-linux

%description addon-vmcore
This package provides plugin which helps to  collect kernel crash information
from vmcore files.

%package addon-pstoreoops
Summary:   Pstore oops addon module for abrt
Requires:  %{name} = %{version}-%{release} abrt-addon-kerneloops
Requires:  %{name}-libs = %{version}-%{release}
Obsoletes: abrt-addon-uefioops < %{version}-%{release}

%description addon-pstoreoops
This package provides plugin which helps to collect kernel oopses from pstore storage.

%package -n python3-abrt-addon
Summary:  Addon module for catching and analyzing Python3 exceptions for abrt
BuildArch: noarch
Requires: %{name} = %{version}-%{release} python3-systemd python3-abrt

%description -n python3-abrt-addon
This package provides python3 hook and python analyzer plugin which helps to handle
uncaught exception in python3 programs.

%package -n python3-abrt-container-addon
Summary:   Container addon for catching Python3 exceptions for abrt
Conflicts: python3-abrt-addon
Requires:  container-exception-logger

%description -n python3-abrt-container-addon
This package provides python3 hook and handling uncaught exception in python3
container's programs.

%package plugin-sosreport
Summary:  Plugin for building automatic sosreports for abrt
Requires: sos >= 3.6 %{name} = %{version}-%{release}

%description plugin-sosreport
This package provides a configuration snippet for abrt events which used to enable
automatic generation of sosreports.

%package plugin-machine-id
Summary:  Plugin to generate machine_id based off dmidecode for abrt
Requires: %{name} = %{version}-%{release}

%description plugin-machine-id
This package provides a configuration snippet for abrt events which used to enable
automatic generation of machine_id.

%package tui
Summary:   Command line interface of abrt
Requires:  %{name} = %{version}-%{release} libreport-cli >= 2.10.0
Requires:  %{name}-libs = %{version}-%{release} abrt-dbus python3-abrt
Requires:  abrt-addon-ccpp python3-argcomplete
Provides:  %{name}-cli-ng = %{version}-%{release}
Obsoletes: %{name}-cli-ng < 2.12.2

%description tui
This package provides a simple command line client for abrt event reports
in command line environment.

%package atomic
Summary:   Package to make easy default installation on Atomic hosts.
Requires:  %{name}-libs = %{version}-%{release}
Conflicts: %{name}-addon-ccpp

%description atomic
This package is used to install all necessary packages for usage from Atomic
hosts.

%package dbus
Summary:  DBus service module of abrt
Requires: %{name} = %{version}-%{release} dbus-tools
Requires: %{name}-libs = %{version}-%{release}

%description dbus
This package provides org.freedesktop.problems API on dbus and uses PolicyKit
to authorize to access the problem data.

%package -n python3-abrt
Summary:  Python3 API module of abrt
Requires: %{name} = %{version}-%{release} %{name}-dbus = %{version}-%{release}
Requires: python3-dbus python3-libreport python3-gobject-base
Requires: %{name}-libs = %{version}-%{release}

%description -n python3-abrt
This package provides high-level API for querying, creating and manipulating
problems handled by ABRT in Python3.

%package -n python3-abrt-doc
Summary:   Python API Documentation of abrt
BuildArch: noarch
Requires:  %{name} = %{version}-%{release} python3-%{name} = %{version}-%{release}

%description -n python3-abrt-doc
This package provides examples and documentation for ABRT Python3 API.

%prep
%global __scm_apply_git(qp:m:) %{__git} am --exclude doc/design --exclude doc/project/abrt.tex

%autosetup -n %{name}-%{version} -p1

%build
autoscan
aclocal
autoconf
automake --add-missing

CFLAGS="%{optflags}" %configure \
        --without-bodhi  \
        --enable-native-unwinder --with-defaultdumplocation=%{_localstatedir}/spool/abrt \
        --enable-doxygen-docs --enable-dump-time-unwind  --disable-silent-rules

%make_build

%install
%make_install PYTHON=%{__python3} \
              dbusabrtdocdir=%{_defaultdocdir}/%{name}-dbus/html/

%find_lang %{name}

find %{buildroot} -name "*.py[co]" -delete

%delete_la_and_a
install -d %{buildroot}%{_localstatedir}/cache/abrt-di
install -d %{buildroot}%{_localstatedir}/lib/abrt
install -d %{buildroot}%{_localstatedir}/run/abrt
install -d %{buildroot}%{_localstatedir}/spool/abrt-upload
install -d %{buildroot}%{_localstatedir}/spool/abrt

desktop-file-install --dir %{buildroot}%{_datadir}/applications \
                     src/applet/org.freedesktop.problems.applet.desktop

ln -sf %{_datadir}/applications/org.freedesktop.problems.applet.desktop \
       %{buildroot}%{_sysconfdir}/xdg/autostart/
ln -sf %{_bindir}/abrt %{buildroot}%{_bindir}/abrt-cli
ln -sf %{_mandir}/man1/abrt.1 %{buildroot}%{_mandir}/man1/abrt-cli.1

%check
make check|| {
    find tests/testsuite.dir -name "testsuite.log" -print -exec cat '{}' \;
    exit 1
}

%pre
%define abrt_gid_uid 173
getent group abrt >/dev/null || groupadd -f -g %{abrt_gid_uid} --system abrt
getent passwd abrt >/dev/null || useradd --system -g abrt -u %{abrt_gid_uid} -d /etc/abrt -s /sbin/nologin abrt
exit 0

%post
%systemd_post abrtd.service

%post addon-ccpp
chown -R abrt:abrt %{_localstatedir}/cache/abrt-di
%systemd_post abrt-journal-core.service
%journal_catalog_update

%post addon-kerneloops
%systemd_post abrt-oops.service
%journal_catalog_update

%post addon-xorg
%systemd_post abrt-xorg.service
%journal_catalog_update

%post -n python3-abrt-addon
%journal_catalog_update

%post addon-vmcore
%systemd_post abrt-vmcore.service
%journal_catalog_update

%post addon-pstoreoops
%systemd_post abrt-pstoreoops.service

%post addon-upload-watch
%systemd_post abrt-upload-watch.service

%preun
%systemd_preun abrtd.service

%preun addon-ccpp
%systemd_preun abrt-journal-core.service

%preun addon-kerneloops
%systemd_preun abrt-oops.service

%preun addon-xorg
%systemd_preun abrt-xorg.service

%preun addon-vmcore
%systemd_preun abrt-vmcore.service

%preun addon-pstoreoops
%systemd_preun abrt-pstoreoops.service

%preun addon-upload-watch
%systemd_preun abrt-upload-watch.service

%postun
%systemd_postun_with_restart abrtd.service

%postun addon-ccpp
%systemd_postun_with_restart abrt-journal-core.service

%postun addon-kerneloops
%systemd_postun_with_restart abrt-oops.service

%postun addon-xorg
%systemd_postun_with_restart abrt-xorg.service

%postun addon-vmcore
%systemd_postun_with_restart abrt-vmcore.service

%postun addon-pstoreoops
%systemd_postun_with_restart abrt-pstoreoops.service

%postun addon-upload-watch
%systemd_postun_with_restart abrt-upload-watch.service

%post gui
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%post atomic
if test -f /etc/abrt/plugins/CCpp.conf; then
    mv /etc/abrt/plugins/CCpp.conf /etc/abrt/plugins/CCpp.conf.rpmsave.atomic || exit 1;
fi

%preun atomic
if test -L /etc/abrt/plugins/CCpp.conf; then
    rm /etc/abrt/plugins/CCpp.conf
fi
if test -f /etc/abrt/plugins/CCpp.conf.rpmsave.atomic; then
    mv /etc/abrt/plugins/CCpp.conf.rpmsave.atomic /etc/abrt/plugins/CCpp.conf || exit 1;
fi

%posttrans
service abrtd condrestart >/dev/null 2>&1 || :

%posttrans addon-ccpp
abrtdir=$(grep "^\s*DumpLocation\b" /etc/abrt/abrt.conf | tail -1 | cut -d'=' -f2 | tr -d ' ')
if test -z "$abrtdir"; then
    abrtdir=%{_localstatedir}/spool/abrt
fi
if test -d "$abrtdir"; then
    for Dir in `find "$abrtdir" -mindepth 1 -maxdepth 1 -type d`
    do
        if test -f "$Dir/analyzer" && grep -q "^CCpp$" "$Dir/analyzer"; then
            /usr/bin/abrt-action-generate-core-backtrace -d "$Dir" -- >/dev/null 2>&1 || :
            test -f "$Dir/core_backtrace" && chown `stat --format=%U:abrt $Dir` "$Dir/core_backtrace" || :
        fi
    done
fi

%posttrans addon-kerneloops
service abrt-oops condrestart >/dev/null 2>&1 || :

%posttrans addon-xorg
service abrt-xorg condrestart >/dev/null 2>&1 || :

%posttrans addon-vmcore
service abrt-vmcore condrestart >/dev/null 2>&1 || :
test -f /etc/abrt/abrt-harvest-vmcore.conf && {
    mv -b /etc/abrt/abrt-harvest-vmcore.conf /etc/abrt/plugins/vmcore.conf
}
exit 0

%posttrans addon-pstoreoops
service abrt-pstoreoops condrestart >/dev/null 2>&1 || :

%posttrans dbus
killall abrt-dbus >/dev/null 2>&1 || :

%files -f %{name}.lang
%doc README.md COPYING
%{_unitdir}/abrtd.service
%{_tmpfilesdir}/abrt.conf
%{_sbindir}/abrtd
%{_sbindir}/abrt-server
%{_sbindir}/abrt-auto-reporting
%{_libexecdir}/abrt-handle-event
%{_libexecdir}/abrt-action-ureport
%{_libexecdir}/abrt-action-save-container-data
%{_bindir}/abrt-watch-log
%{_bindir}/abrt-handle-upload
%{_bindir}/abrt-action-notify
%{_bindir}/abrt-action-save-package-data
%{_bindir}/abrt-action-analyze-python
%{_bindir}/abrt-action-analyze-xorg
%dir %attr(0751, root, abrt) %{_localstatedir}/spool/abrt
%dir %attr(0700, abrt, abrt) %{_localstatedir}/spool/%{name}-upload
%dir %attr(0755, root, root) %{_localstatedir}/run/%{name}
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/org.freedesktop.problems.daemon.conf
%config(noreplace) %{_sysconfdir}/abrt/abrt.conf
%config(noreplace) %{_sysconfdir}/abrt/abrt-action-save-package-data.conf
%config(noreplace) %{_sysconfdir}/abrt/gpg_keys.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/abrt_event.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/smart_event.conf
%ghost %attr(0666, -, -) %{_localstatedir}/run/%{name}/abrt.socket
%ghost %attr(0644, -, -) %{_localstatedir}/run/%{name}/abrtd.pid
%exclude %{_infodir}/dir
%exclude %{_sysconfdir}/profile.d/abrt-console-notification.sh

%files libs
%{_libdir}/libabrt.so.*
%dir %{_sysconfdir}/%{name}
%dir %{_sysconfdir}/%{name}/plugins
%dir %{_datadir}/%{name}
%{_datadir}/augeas/lenses/abrt.aug

%files devel
%doc apidoc/html/*.{html,png,css,js}
%{_includedir}/abrt/{abrt-dbus,hooklib,libabrt,problem_api}.h
%{_libdir}/libabrt.so
%{_libdir}/pkgconfig/abrt.pc

%files gui-devel
%{_includedir}/abrt/abrt-config-widget.h
%{_includedir}/abrt/system-config-abrt.h
%{_libdir}/libabrt_gui.so
%{_libdir}/pkgconfig/abrt_gui.pc

%files gui
%dir %{_datadir}/%{name}
%{_libdir}/libabrt_gui.so.*
%{_bindir}/abrt-applet
%{_bindir}/system-config-abrt
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/%{name}/ui/*
%{_datadir}/applications/org.freedesktop.problems.applet.desktop
%{_datadir}/dbus-1/services/org.freedesktop.problems.applet.service
%config(noreplace) %{_sysconfdir}/xdg/autostart/org.freedesktop.problems.applet.desktop

%files addon-ccpp
%dir %{_localstatedir}/lib/abrt
%dir %attr(0775, abrt, abrt) %{_localstatedir}/cache/abrt-di
%{_libexecdir}/abrt-gdb-exploitable
%{_journalcatalogdir}/abrt_ccpp.catalog
%{_unitdir}/abrt-journal-core.service

%attr(6755, abrt, abrt) %{_libexecdir}/abrt-action-install-debuginfo-to-abrt-cache

%{_bindir}/abrt-action-analyze-c
%{_bindir}/abrt-action-trim-files
%{_bindir}/abrt-action-analyze-core
%{_bindir}/abrt-action-analyze-vulnerability
%{_bindir}/abrt-action-install-debuginfo
%{_bindir}/abrt-action-generate-backtrace
%{_bindir}/abrt-action-generate-core-backtrace
%{_bindir}/abrt-action-analyze-backtrace
%{_bindir}/abrt-action-list-dsos
%{_bindir}/abrt-action-perform-ccpp-analysis
%{_bindir}/abrt-action-analyze-ccpp-local
%{_bindir}/abrt-dump-journal-core
%config(noreplace) %{_sysconfdir}/%{name}/plugins/CCpp.conf
%config(noreplace) %{_sysconfdir}/libreport/plugins/catalog_journal_ccpp_format.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/ccpp_event.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/gconf_event.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/vimrc_event.conf
%{_datadir}/libreport/events/analyze_CCpp.xml
%{_datadir}/libreport/events/analyze_LocalGDB.xml
%{_datadir}/libreport/events/analyze_RetraceServer.xml
%{_datadir}/libreport/events/collect_*.xml
%{_datadir}/libreport/events/post_report.xml

%files addon-upload-watch
%{_sbindir}/abrt-upload-watch
%{_unitdir}/abrt-upload-watch.service

%files retrace-client
%{_bindir}/abrt-retrace-client
%config(noreplace) %{_sysconfdir}/libreport/events.d/ccpp_retrace_event.conf

%files addon-kerneloops
%{_unitdir}/abrt-oops.service
%{_journalcatalogdir}/abrt_koops.catalog
%dir %{_localstatedir}/lib/abrt
%config(noreplace) %{_sysconfdir}/libreport/events.d/koops_event.conf
%config(noreplace) %{_sysconfdir}/libreport/plugins/catalog_koops_format.conf
%config(noreplace) %{_sysconfdir}/%{name}/plugins/oops.conf
%{_bindir}/abrt-dump-*oops
%{_bindir}/abrt-action-analyze-oops

%files addon-xorg
%{_bindir}/abrt-dump-*xorg
%{_unitdir}/abrt-xorg.service
%{_journalcatalogdir}/abrt_xorg.catalog
%config(noreplace) %{_sysconfdir}/libreport/events.d/xorg_event.conf
%config(noreplace) %{_sysconfdir}/libreport/plugins/catalog_xorg_format.conf
%config(noreplace) %{_sysconfdir}/%{name}/plugins/xorg.conf

%files addon-vmcore
%{_unitdir}/abrt-vmcore.service
%config(noreplace) %{_sysconfdir}/%{name}/plugins/vmcore.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/vmcore_event.conf
%config(noreplace) %{_sysconfdir}/libreport/plugins/catalog_vmcore_format.conf
%{_datadir}/libreport/events/analyze_VMcore.xml
%{_sbindir}/abrt-harvest-vmcore
%{_bindir}/abrt-action-analyze-vmcore
%{_bindir}/abrt-action-check-*
%{_journalcatalogdir}/abrt_vmcore.catalog

%files addon-pstoreoops
%{_unitdir}/abrt-pstoreoops.service
%{_sbindir}/abrt-harvest-pstoreoops
%{_bindir}/abrt-merge-pstoreoops

%files -n python3-abrt-addon
%{_journalcatalogdir}/python3_abrt.catalog
%config(noreplace) %{_sysconfdir}/%{name}/plugins/python3.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/python3_event.conf
%config(noreplace) %{_sysconfdir}/libreport/plugins/catalog_python3_format.conf
%{python3_sitelib}/abrt3.pth
%{python3_sitelib}/abrt_exception_handler3.py
%{python3_sitelib}/__pycache__/abrt_exception_handler3.*

%files -n python3-abrt-container-addon
%{python3_sitelib}/abrt3_container.pth
%{python3_sitelib}/abrt_exception_handler3_container.py
%{python3_sitelib}/__pycache__/abrt_exception_handler3_container.*

%files plugin-sosreport
%config(noreplace) %{_sysconfdir}/libreport/events.d/sosreport_event.conf

%files plugin-machine-id
%config(noreplace) %{_sysconfdir}/libreport/events.d/machine-id_event.conf
%{_libexecdir}/abrt-action-generate-machine-id

%files tui
%config(noreplace) %{_sysconfdir}/bash_completion.d/abrt.bash_completion
%{_bindir}/abrt
%{_bindir}/abrt-cli
%{python3_sitelib}/abrtcli/

%files atomic
%config(noreplace) %{_sysconfdir}/%{name}/abrt-action-save-package-data.conf
%{_bindir}/abrt-action-save-package-data

%files dbus
%{_sbindir}/abrt-dbus
%config(noreplace) %{_sysconfdir}/dbus-1/system.d/dbus-abrt.conf
%config(noreplace) %{_sysconfdir}/libreport/events.d/abrt_dbus_event.conf
%{_datadir}/dbus-1/interfaces/*
%{_datadir}/dbus-1/system-services/org.freedesktop.problems.service
%{_datadir}/polkit-1/actions/abrt_polkit.policy
%dir %{_defaultdocdir}/%{name}-dbus/
%dir %{_defaultdocdir}/%{name}-dbus/html/
%{_defaultdocdir}/%{name}-dbus/html/*.html
%{_defaultdocdir}/%{name}-dbus/html/*.css

%files -n python3-abrt
%{python3_sitearch}/problem/

%files -n python3-abrt-doc
%{python3_sitelib}/problem_examples

%files help
%{_mandir}/man*/*

%changelog
* Sun Sep 20 2020 leiju <leiju4@huawei.com> - 2.14.2-2
- fix uninstall abrt-addon-ccpp error

* Fri Aug 07 2020 zhangjiapeng <zhangjiapeng9@huawei.com> - 2.14.2-1
- update to 2.14.2

* Thu Jul 23 2020 wutao <wutao61@huawei.com> - 2.13.0-7
- fix build error because of automake updating

* Mon Mar 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.13.0-6
- fix build error

* Mon Mar 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.13.0-5
- remove useless packages

* Mon Feb 17 2020 hexiujun <hexiujun1@huawei.com> - 2.13.0-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:unpack libs subpackage

* Thu Feb 13 2020 gulining<gulining1@huawei.com> 2.13.0-3
- resolve build failed

* Sat Nov 30 2019 yanzhihua <yanzhihua4@huawei.com> 2.13.0-2
- Package init
